import { Directive, ElementRef, HostListener } from "@angular/core";
import { environment } from "src/environments/environment";

@Directive({
  selector: "img[imgPersonError]",
})
export class ImgPersonErrorDirective {
  constructor(private el: ElementRef) {}

  @HostListener("error")
  private onError() {
    this.el.nativeElement.src = environment.urlNoImgPerson;


  }
}

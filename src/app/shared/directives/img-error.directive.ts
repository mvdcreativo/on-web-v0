import { Directive, ElementRef, HostListener } from "@angular/core";
import { environment } from "src/environments/environment";

@Directive({
  selector: "img[imgError]",
})
export class ImgErrorDirective {
  constructor(private el: ElementRef) {}

  @HostListener("error")
  private onError() {
    this.el.nativeElement.src = "src/assets/img/logo.svg";


  }
}

export const environment = {
  production: true,
  API: "https://api.oncapacitaciones.com/api/",
  AuthAPI: 'https://api.oncapacitaciones.com/api/auth/',
  urlImages: "https://api.oncapacitaciones.com/storage/images/",
  urlNoImgPerson: "https://api.oncapacitaciones.com/storage/images/users/larg/no-person-img.svg"

};
